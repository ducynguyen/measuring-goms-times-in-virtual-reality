﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PhysicalButtonTrigger : MonoBehaviour
{
    public static event Action onPhysicalButtonPressed;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Button")
            onPhysicalButtonPressed?.Invoke();
    }
}
