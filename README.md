In our work, we developed an approach to measure the execution times for typical interaction elements in Virtual Reality using the Oculus Quest. For this, we conducted an experiment with 18 participants. In the experiment, the participants performed different scenarios of the focused interactions, in seven scenes. The interactions we focused on are: 

- Grab and drop with the right controller
- Teleportation with the right controller
- Selection via ray cast with the right controller

This project contains the scenes for the time measurement the interactions.

The video recordings of each experiment sessions and the tables with the measured values have been published at:
https://zenodo.org/record/6904018#.YuJV1HZBxPY
